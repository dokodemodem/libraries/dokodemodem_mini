/*
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 *
 * reference codes
 * https://github.com/adafruit/Adafruit_SleepyDog
 */

#include "dokodemo.h"

MCP3221 adcDm = MCP3221();
RX8130 rtcDm = RX8130();
BME280 SensorDm = BME280();

// USB Uart      : SerialUSB/SerialDebug
// Modem UART    : Serial1/UartModem
// External UART : Serial/UartExternal
// Internal I2C  : Wire/I2C_internal

// External I2C
// variant.cpp and variant.h should be modify
// https://gitlab.com/dokodemodem/libraries/dokodemo_mini
TwoWire I2C_external(&sercom2, EXTERNAL_SDA, EXTERNAL_SCL);
void SERCOM2_Handler()
{
    I2C_external.onService();
}

DOKODEMO::DOKODEMO()
{
    _wdt_initialized = false;
    _sleep_initialized = false;
}

bool DOKODEMO::begin()
{
    // Port setting
    // Power off switch
    pinMode(MAIN_PWR_CNT_OUT, INPUT);

    // exPower control
    digitalWrite(EX_POWER_PORT, LOW);
    pinMode(EX_POWER_PORT, OUTPUT);

    // Modem power control
    digitalWrite(MODEM_POWER_PORT, LOW);
    pinMode(MODEM_POWER_PORT, OUTPUT);

    // Battery check swich
    digitalWrite(BATMON_ONOFF, LOW);
    pinMode(BATMON_ONOFF, OUTPUT);

    // Battery check Port
    analogReference(AR_INTERNAL1V65);
    pinMode(BATT_AD, INPUT);

    // External Serial power
    digitalWrite(EXCOM_ONOFF, LOW);
    pinMode(EXCOM_ONOFF, OUTPUT);

    // SD Power
    digitalWrite(SD_PW, LOW);
    pinMode(SD_PW, OUTPUT);

    // GPIO
    digitalWrite(EXGPIO_PORT_OUTPUT_0, LOW);
    pinMode(EXGPIO_PORT_OUTPUT_0, OUTPUT);
    pinMode(EXGPIO_PORT_INPUT_0, INPUT);

    // LED
    digitalWrite(RED_LED, LOW);
    pinMode(RED_LED, OUTPUT);
    digitalWrite(GREEN_LED, LOW);
    pinMode(GREEN_LED, OUTPUT);

    // Power Up reason 1: from RTC, 0:normal boot
    pinMode(POW_STA_IN, INPUT);

    // DIP2
    pinMode(SW_STA_IN, INPUT);

    // Volume control
    digitalWrite(VOLUME_EN1, LOW);
    pinMode(VOLUME_EN1, OUTPUT);

    // E2PROM WriteProtect
    digitalWrite(WP_CNT_OUT, HIGH);
    pinMode(WP_CNT_OUT, OUTPUT);

    // for LTE-M
    pinMode(RF_CTS_IN, INPUT);
    pinMode(VDD_GPIO_OUT, INPUT);

    // Start internal I2C
    I2C_internal.begin(); // 100kHz frequency

    // BME280
    SensorDm.setI2CAddress(BME280_I2C_ADD);
    if (SensorDm.beginI2C(I2C_internal) == false)
    {
        SerialDebug.println("Sensor connect failed");
        return false;
    }

    // ADC
    adcDm.begin(MCP3221_I2C_ADD, I2C_internal);

    // RTC
    bool status = rtcDm.begin(RX8130_I2C_ADD, I2C_internal);
    if (status == false)
    {
        SerialDebug.println("Could not find a valid rtcDm8130, check wiring!");
        return false;
    }
    else
    {
        rtcValidAtBoot = rtcDm._valid;
    }

    return true;
}

bool DOKODEMO::LedCtrl(uint8_t led, uint8_t onoff)
{
    if ((led != RED_LED && led != GREEN_LED) || onoff > 1)
    {
        return false;
    }

    // !!! ON/HIGH ->turn off, OFF/LOW-> turn on , so invert it.
    onoff = (~onoff) & 0x01;

    digitalWrite(led, onoff);

    return true;
}

bool DOKODEMO::ModemPowerCtrl(uint8_t onoff)
{
    if (onoff != ON && onoff != OFF)
    {
        return false;
    }

    digitalWrite(MODEM_POWER_PORT, onoff);

#ifdef USE_LTE_M
    delay(1);
    if (onoff)
    {
        digitalWrite(VDD_GPIO_OUT, HIGH);
        pinMode(VDD_GPIO_OUT, OUTPUT);
    }
    else
    {
        pinMode(VDD_GPIO_OUT, INPUT);
    }
#endif

    return true;
}

float DOKODEMO::readPressure()
{
    return SensorDm.readFloatPressure();
}

float DOKODEMO::readHumidity()
{
    return SensorDm.readFloatHumidity();
}

float DOKODEMO::readTemperature()
{
    return SensorDm.readTempC();
}

float DOKODEMO::readExADC()
{
    return adcDm.readADC();
}

int DOKODEMO::readExIN()
{
    return digitalRead(EXGPIO_PORT_INPUT_0) ? 0 : 1;
}

bool DOKODEMO::exOutCtrl(uint8_t onoff)
{
    if (onoff > 1)
    {
        return false;
    }

    digitalWrite(EXGPIO_PORT_OUTPUT_0, onoff);

    return true;
}

void DOKODEMO::setPwm(uint16_t helz)
{
    tone(BEEP_PWM, helz);
}

bool DOKODEMO::BeepVolumeCtrl(uint8_t onoff)
{
    if (onoff != ON && onoff != OFF)
    {
        return false;
    }

    if (onoff)
    {
        digitalWrite(VOLUME_EN1, 1);
    }
    else
    {
        digitalWrite(VOLUME_EN1, 0);
    }

    return true;
}

bool DOKODEMO::exPowerCtrl(uint8_t onoff)
{
    if (onoff != ON && onoff != OFF)
    {
        return false;
    }

    digitalWrite(EX_POWER_PORT, onoff);

    return true;
}

bool DOKODEMO::exComPowerCtrl(uint8_t onoff)
{
    if (onoff != ON && onoff != OFF)
    {
        return false;
    }

    digitalWrite(EXCOM_ONOFF, onoff);

    return true;
}

DateTime DOKODEMO::rtcNow()
{
    return rtcDm.now();
}

void DOKODEMO::rtcAdjust(const DateTime &dt)
{
    rtcDm.adjust(dt);
    rtcAdjusted = true;
}

bool DOKODEMO::sleep(SleepMode mode)
{
    bool rv = false;

    switch (mode)
    {
    case SleepMode::IDLE0:
    case SleepMode::IDLE1:
    case SleepMode::IDLE2:
        SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk;
        PM->SLEEP.reg = (uint8_t)mode;
        rv = true;
        break;
    case SleepMode::DEEPSLEEP:
        // for powersave
        SensorDm.setMode(MODE_SLEEP);

        SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
        rv = true;
        break;
    default:
        break;
    }

    if (rv)
    {
        // Disable systick interrupt: errata 1.5.7 Potential Lockup on Standby Entry
        if (mode == SleepMode::DEEPSLEEP)
            SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk;
        __DSB();
        __WFI();
        // Enable systick interrupt
        if (mode == SleepMode::DEEPSLEEP)
            SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;

        if (mode == SleepMode::DEEPSLEEP)
        {
            SensorDm.setMode(MODE_NORMAL);
        }
    }

    return rv;
}

void DOKODEMO::setRtcTimer(const RtcTimerPeriod period, const uint16_t count, voidFuncPtr callback)
{
    rtcDm.setTimer(period, count);

    if (callback != nullptr)
    {
        attachInterrupt(RTC_IRQ, callback, RISING);
    }
    else
    {
        attachInterrupt((RTC_IRQ), nullptr, RISING);
    }

    _initialize_sleep();
}

bool DOKODEMO::chkRtcTimer(void)
{
    return rtcDm.chkTimer();
}

void DOKODEMO::stopRtcTimer(void)
{
    detachInterrupt(digitalPinToInterrupt(RTC_IRQ));
    rtcDm.stopTimer();
}

float DOKODEMO::readBatteryVoltage(void)
{
    float rv = 0;

    digitalWrite(BATMON_ONOFF, HIGH);
    delay(20); // wait charge
    int volt = analogRead(BATT_AD);
    digitalWrite(BATMON_ONOFF, LOW);

    rv = (float)volt / 1024.0 * 1.65 * 10.0;
    rv += 0.2; // add diode loss value

    return rv;
}

void DOKODEMO::writeRtcRam(uint32_t val)
{
    rtcDm.writeRAM(val);
}

uint32_t DOKODEMO::readRtcRam(void)
{
    return rtcDm.readRAM();
}

void DOKODEMO::setAlarm(uint8_t mode, uint8_t hour, uint8_t min)
{
    rtcDm.setAlarm(mode, hour, min);
}

void DOKODEMO::stopAlarm(void)
{
    rtcDm.stopAlarm();
}

void DOKODEMO::clearAlarm(void)
{
    rtcDm.clearAlarm();
}

void DOKODEMO::reset(void)
{
    SCB->AIRCR = ((0x5FA << SCB_AIRCR_VECTKEY_Pos) | SCB_AIRCR_SYSRESETREQ_Msk);
}

int DOKODEMO::wdt_enable(int maxPeriodMS)
{
    int cycles;
    uint8_t bits;

    if (!_wdt_initialized)
        _initialize_wdt();

    WDT->CTRL.reg = 0; // Disable watchdog for config
    while (WDT->STATUS.bit.SYNCBUSY)
        ;

    if ((maxPeriodMS >= 16000) || !maxPeriodMS)
    {
        cycles = 16384;
        bits = 0xB;
    }
    else
    {
        cycles = (maxPeriodMS * 1024L + 500) / 1000; // ms -> WDT cycles
        if (cycles >= 8192)
        {
            cycles = 8192;
            bits = 0xA;
        }
        else if (cycles >= 4096)
        {
            cycles = 4096;
            bits = 0x9;
        }
        else if (cycles >= 2048)
        {
            cycles = 2048;
            bits = 0x8;
        }
        else if (cycles >= 1024)
        {
            cycles = 1024;
            bits = 0x7;
        }
        else if (cycles >= 512)
        {
            cycles = 512;
            bits = 0x6;
        }
        else if (cycles >= 256)
        {
            cycles = 256;
            bits = 0x5;
        }
        else if (cycles >= 128)
        {
            cycles = 128;
            bits = 0x4;
        }
        else if (cycles >= 64)
        {
            cycles = 64;
            bits = 0x3;
        }
        else if (cycles >= 32)
        {
            cycles = 32;
            bits = 0x2;
        }
        else if (cycles >= 16)
        {
            cycles = 16;
            bits = 0x1;
        }
        else
        {
            cycles = 8;
            bits = 0x0;
        }
    }

    WDT->INTENCLR.bit.EW = 1;   // Disable early warning interrupt
    WDT->CONFIG.bit.PER = bits; // Set period for chip reset
    WDT->CTRL.bit.WEN = 0;      // Disable window mode
    while (WDT->STATUS.bit.SYNCBUSY)
        ; // Sync CTRL write

    wdt_reset();              // Clear watchdog interval
    WDT->CTRL.bit.ENABLE = 1; // Start watchdog now!
    while (WDT->STATUS.bit.SYNCBUSY)
        ;

    return (cycles * 1000L + 512) / 1024; // WDT cycles -> ms
}

void DOKODEMO::wdt_reset()
{
    while (WDT->STATUS.bit.SYNCBUSY)
        ;
    WDT->CLEAR.reg = WDT_CLEAR_CLEAR_KEY;
}

void DOKODEMO::wdt_disable(void)
{
    WDT->CTRL.bit.ENABLE = 0;
    while (WDT->STATUS.bit.SYNCBUSY)
        ;
}
void WDT_Handler(void)
{
    WDT->CTRL.bit.ENABLE = 0; // Disable watchdog
    while (WDT->STATUS.bit.SYNCBUSY)
        ;                    // Sync CTRL write
    WDT->INTFLAG.bit.EW = 1; // Clear interrupt flag
}

void DOKODEMO::_initialize_wdt()
{
    // Generic clock generator 2, divisor = 32 (2^(DIV+1))
    GCLK->GENDIV.reg = GCLK_GENDIV_ID(2) | GCLK_GENDIV_DIV(4);
    // Enable clock generator 2 using low-power 32KHz oscillator.
    // With /32 divisor above, this yields 1024Hz(ish) clock.
    GCLK->GENCTRL.reg = GCLK_GENCTRL_ID(2) | GCLK_GENCTRL_GENEN |
                        GCLK_GENCTRL_SRC_OSCULP32K | GCLK_GENCTRL_DIVSEL;
    while (GCLK->STATUS.bit.SYNCBUSY)
        ;
    // WDT clock = clock gen 2
    GCLK->CLKCTRL.reg =
        GCLK_CLKCTRL_ID_WDT | GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK2;

    // Enable WDT early-warning interrupt
    NVIC_DisableIRQ(WDT_IRQn);
    NVIC_ClearPendingIRQ(WDT_IRQn);
    NVIC_SetPriority(WDT_IRQn, 0); // Top priority
    NVIC_EnableIRQ(WDT_IRQn);

    _wdt_initialized = true;
}

void DOKODEMO::_initialize_sleep()
{
    SYSCTRL->XOSC32K.reg |= (SYSCTRL_XOSC32K_RUNSTDBY | SYSCTRL_XOSC32K_ONDEMAND); // set external 32k oscillator to run when idle or sleep mode is chosen
    REG_GCLK_CLKCTRL |= GCLK_CLKCTRL_ID(GCM_EIC) |                                 // generic clock multiplexer id for the external interrupt controller
                        GCLK_CLKCTRL_GEN_GCLK1 |                                   // generic clock 1 which is xosc32k
                        GCLK_CLKCTRL_CLKEN;                                        // enable it
    while (GCLK->STATUS.bit.SYNCBUSY)
        ;

    // errata 1.14.2 Power-down Modes and Wake-up From Sleep
    NVMCTRL->CTRLB.bit.SLEEPPRM = NVMCTRL_CTRLB_SLEEPPRM_DISABLED_Val;

    _sleep_initialized = true;
}

bool DOKODEMO::e2prom_write(uint16_t add, uint8_t data)
{
    bool rv = false;

    if (add < 8192)
    {
        digitalWrite(WP_CNT_OUT, LOW); // Write Protect off for E2PROM

        I2C_internal.beginTransmission(BR24G64_I2C_ADD);
        I2C_internal.write((uint8_t)(add >> 8));   // MSB
        I2C_internal.write((uint8_t)(add & 0xFF)); // LSB
        I2C_internal.write(data);
        I2C_internal.endTransmission();

        delay(5); // for stable

        digitalWrite(WP_CNT_OUT, HIGH); // set Write Protect for E2PROM

        rv = true;
    }

    return rv;
}

bool DOKODEMO::e2prom_read(uint16_t add, uint8_t *data)
{
    bool rv = false;

    if (add < 8192)
    {
        byte rdata = 0xFF;

        I2C_internal.beginTransmission(BR24G64_I2C_ADD);
        I2C_internal.write((uint8_t)(add >> 8));   // MSB
        I2C_internal.write((uint8_t)(add & 0xFF)); // LSB
        I2C_internal.endTransmission();

        I2C_internal.requestFrom(BR24G64_I2C_ADD, 1);
        if (I2C_internal.available())
        {
            rdata = I2C_internal.read();
            rv = true;
        }

        *data = rdata;
    }

    return rv;
}

uint8_t DOKODEMO::readDipSW()
{
    return digitalRead(SW_STA_IN);
}
