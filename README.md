# dokodemo_mini
サーキットデザイン製プログラマブル無線ユニット「どこでもでむmini」用のライブラリです。

platformioで新プロジェクトを作るときは" Arduino M0"を指定してください。

## 注意事項

Arduinoとのハードウェアの違いに対応するため、下記フォルダにあるvariant.hとvariant.cppを変更してください。

> C:\Users\\********\\.platformio\packages\framework-arduino-samd\variants\arduino_mzero\variant.h

> C:\Users\\********\\.platformio\packages\framework-arduino-samd\variants\arduino_mzero\variant.cpp


1.variant.hの変更部分

LED宣言関連をコメントアウトする。
```
// LEDs
#if 0
#define PIN_LED_13           (13u)
#define PIN_LED_RXL          (30u)
#define PIN_LED_TXL          (31u)
#define PIN_LED              PIN_LED_13
#define PIN_LED2             PIN_LED_RXL
#define PIN_LED3             PIN_LED_TXL
#define LED_BUILTIN          PIN_LED_13
#endif
```


2.variant.cppの変更部分

*外部I2Cを使用するために下記部分を、
```
 { PORTA,  8, PIO_TIMER,
 { PORTA,  9, PIO_TIMER,
```
下記に変更
```
 { PORTA,  8, PIO_SERCOM_ALT,
 { PORTA,  9, PIO_SERCOM_ALT, 
```
